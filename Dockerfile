FROM python:3.8-slim-buster

RUN apt-get update -y \
    && apt-get install apt-transport-https ca-certificates gnupg curl -y \
    && echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list \ 
    && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - \
    && apt-get update -y \
    && apt-get install google-cloud-sdk -y
RUN apt-get install google-cloud-sdk-app-engine-python -y && rm -rf /var/lib/apt/lists/*


